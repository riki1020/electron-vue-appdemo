export default {
  state: {
    userInfo: null
  },
  mutations: {
    setUserInfo (state, val) {
      state.userInfo = val
    }
  },
  actions: {
    // 登录
    async fetchUserInfo (ctx, params) {
      const {data: res} = await ctx.dispatch('fetchProxy', {url: '/userinfo', params})
      if (res.res === 'SUCCESS') {
        ctx.commit('setUserInfo', res.data)
      }
    },
    // 退出
    async logout (ctx, params) {
      await ctx.dispatch('fetchProxy', {url: '/logout', params})
    }
  }
}