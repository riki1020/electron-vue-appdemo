import axios from 'axios'


axios.defaults.baseURL = 'http://localhost:8001';

export default ({url, params, config = {}}) => {
  console.log(url, params)
  return axios({
    method: config.method || 'post',
    url,
    data: params,
    responseType: 'json'
  })
}