import Vue from 'vue'
import Vuex from 'vuex'

import modules from './modules'
import fetchProxy from './fetchProxy.js'

Vue.use(Vuex)

export default new Vuex.Store({
  actions: {
    fetchProxy: (ctx, params) => fetchProxy(params)
  },
  modules,
  strict: process.env.NODE_ENV !== 'production'
})
